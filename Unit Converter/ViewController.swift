//
//  ViewController.swift
//  Unit Converter
//
//  Created by GDD Student on 9/5/16.
//  Copyright © 2016 GDD Student. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UIPickerViewDelegate{
    
    let userDefaultsLastRowKey = "defaultCelciusPickerRow"
    
    
    @IBOutlet var temperatureRange: TemperatureRange!
    
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var celciusPicker: UIPickerView!
    
    let converter = UnitConverter()
    
    func initialPickerRow() -> Int {
        let savedRow = NSUserDefaults.standardUserDefaults().objectForKey(userDefaultsLastRowKey) as? Int
        
        if let row = savedRow{
            return row
        }else {
            return celciusPicker.numberOfRowsInComponent(0)/2
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let defaultPickerRow = celciusPicker.numberOfRowsInComponent(0)/2
        celciusPicker.selectRow(defaultPickerRow, inComponent:0, animated: false)
        pickerView(celciusPicker, didSelectRow: defaultPickerRow, inComponent: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pickerView(pickerView : UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        let celciusValue = temperatureRange.values[row]
        return "\(celciusValue) ºC"
    }
    
    
    func displayConvertedTemperatureForRow(row:Int){
        let degreesCelcius = temperatureRange.values[row]
        temperatureLabel.text = "\(Int(converter.degreesFahrenheit(degreesCelcius)))℉"
    }
    
    //save last chosen temp
    
    func saveSelectedRow(row: Int){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(row, forKey: "defaultCelciusPickerRow")
        defaults.synchronize()
    }
    
    func pickerView(pickerView : UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        displayConvertedTemperatureForRow(row)
        saveSelectedRow(row)
    }
    
    
}

