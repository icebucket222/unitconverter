//
//  TemperatureRange.swift
//  Unit Converter
//
//  Created by gddstudent on 10/5/16.
//  Copyright © 2016 GDD Student. All rights reserved.
//

import UIKit

class TemperatureRange: NSObject, UIPickerViewDataSource
{
    let values = (-100...100).map {$0}
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return values.count
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
}
