//
//  UnitConverter.swift
//  Unit Converter
//
//  Created by gddstudent on 10/5/16.
//  Copyright © 2016 GDD Student. All rights reserved.
//

import Foundation

class UnitConverter
{
    func degreesFahrenheit(degreesCelcius: Int) -> Int
    {
        return Int(1.8 * Float(degreesCelcius) + 32.0)
    }
}